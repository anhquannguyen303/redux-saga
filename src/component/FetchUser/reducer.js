import * as types from './constans';
import produce from 'immer';

const initialState = {
    userList: [],
    loading: false,
    message: "",
    user: undefined,
};

const userReducer = (state = initialState, action) =>
    produce(state, (draft) => {
        switch (action.type) {
            case types.FETCH_USER:
                draft.loading = true;
                break;
            case types.FETCH_USER_SUCCESS:
                draft.loading = false;
                draft.userList = action.payload;
                break;
            case types.FETCH_USER_FAILED:
                draft.loading = false;
                draft.message = action.payload;
                break;
            case types.GET_USER:
                draft.loading = true;
                break;
            case types.GET_USER_SUCCESS:
                draft.loading = false;
                draft.user = action.payload;
                break;
            case types.GET_USER_FAILED:
                draft.loading = false;
                draft.message = action.payload;
                break;
            case types.DELETE_USER:
                draft.loading = true;
                break;
            case types.DELETE_USER_SUCCESS:
                draft.loading = false;
                draft.message = action.payload;
                break;
            case types.DELETE_USER_FAILED:
                draft.loading = false;
                draft.message = action.payload;
                break;
        }
    });

export default userReducer;
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux';
import {DeleteUser, FetchData, GetUser} from './action';

function User() {
    const userList = useSelector(state => state.user.userList);
    console.log(userList);
    const dispatch = useDispatch();
    //const [id, setId] = useState(2);

    useEffect(() => {
        const data = {
            id: 3,
        }
        dispatch(DeleteUser(data));
        //setId(id + 1)
    }, []);

    const fec = () =>{   
        dispatch(FetchData());
    }

    const getUser = () =>{
        const data = {
            id: 27,
        }
        dispatch(GetUser(data))
    }

    return (
        <>
            <div>User List</div>
            <button onClick={fec}>getAllUser</button> <br />
            <button onClick={getUser}>getUser</button>
        </>
    )
}

export default User;
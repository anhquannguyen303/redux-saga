import * as types from './constans';

export function defaultAction() {
    return {
      type: types.DEFAULT_ACTION,
    };
  }
  
  export function FetchData(payload) {
    return {
      type: types.FETCH_USER,
      payload
    }
  }

  export function FetchDataSuccess(payload) {
    return {
      type: types.FETCH_USER_SUCCESS,
      payload
    }
  }

  export function FetchDataFailed(payload) {
    return {
      type: types.FETCH_USER_FAILED,
      payload
    }
  }

  export function GetUser(payload) {
    console.log(payload)
    return {
      type: types.GET_USER,
      payload
    }
  }

  export function GetUserSuccess(payload) {
    return {
      type: types.GET_USER_SUCCESS,
      payload
    }
  }

  export function GetUserFailed(payload) {
    return {
      type: types.GET_USER_FAILED,
      payload
    }
  }

  export function DeleteUser(payload) {
    console.log(payload)
    return {
      type: types.DELETE_USER,
      payload
    }
  }

  export function DeleteUserSuccess(payload) {
    return {
      type: types.DELETE_USER_SUCCESS,
      payload
    }
  }

  export function DeleteUserFailed(payload) {
    return {
      type: types.DELETE_USER_FAILED,
      payload
    }
  }
  
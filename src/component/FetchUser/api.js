import baseApiUrl from '../../baseAPI';

export const apiFetchData = (data) => {
    return new Promise((resolve, reject) => {
        return baseApiUrl
            .get(`${data[0]}`, data[1])
            .then((res) => resolve(res))
            .catch((err) => reject(err));
    });
}

export const apiDeleteData = (data) => {
    return new Promise((resolve, reject) => {
        return baseApiUrl
            .delete(`${data[0]}`, data[1])
            .then((res) => resolve(res))
            .catch((err) => reject(err));
    });
}



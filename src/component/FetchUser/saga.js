
import { apiFetchData, apiDeleteData } from './api';
import * as types from './constans';
import { take, call, put, select, takeEvery } from 'redux-saga/effects';
import { DeleteUserFailed, FetchDataFailed, FetchDataSuccess, GetUserFailed } from './action';

export function* fetchUser({payload}){
    try {
        const res = yield call(apiFetchData, ['user']);
        console.log(res);
        if(res.status === 200){
            yield put(FetchDataSuccess(res.data));
        }else{
            yield put(FetchDataFailed("loi"));
        }
    } catch (error) {
        console.log(error.message)
        yield put(FetchDataFailed(error.message))
    }
}

export function* getUser({payload}){
    console.log(payload)
    try{
        const res = yield call(apiFetchData, [`user/${payload.id}`]);
        console.log(res)
    }catch (error){
        yield put(GetUserFailed(error.message))
    }

}

export function* deleteUser({payload}){
    console.log(payload)
    try{
        const res = yield call(apiDeleteData, [`user/${payload.id}`]);
        console.log(res)
    }catch (error){
        yield put(DeleteUserFailed(error.message))
    }

}

export default function* saga(){
    yield takeEvery(types.FETCH_USER, fetchUser);
    yield takeEvery(types.GET_USER, getUser);
    yield takeEvery(types.DELETE_USER, deleteUser);
}
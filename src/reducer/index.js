import {combineReducers} from 'redux';
import { useReducer } from 'react';
import userReducer from '../component/FetchUser/reducer';

const rootReduce = combineReducers({
    user: userReducer,
});

export default rootReduce; 
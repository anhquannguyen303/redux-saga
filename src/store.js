import { applyMiddleware, createStore } from "redux";
import rootReduce from "./reducer";
import createSagaMiddleware from 'redux-saga';
import dataSaga from './component/FetchUser/saga';


const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReduce, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(dataSaga);

export default store;